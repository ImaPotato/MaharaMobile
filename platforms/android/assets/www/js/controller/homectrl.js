angular.module('Mahara').controller('HomeCtrl', function($scope, $location, $controller) {
  $controller('BaseCtrl', { $scope: $scope });

  $scope.loadSettings();

  if($scope.settings.advanced.lastsynctime == 0){
    _.defer(function() {
      $scope.$apply(function() {
        $location.path("/login");
      });
    });
  }

});
